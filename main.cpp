#include <iostream>
#include <vector>
#include <cstdlib>
#include <cmath>

using namespace std;

int main() {
    int n, r, max, min;
    long int sum;
    cout << "n: ";
    cin >> n;
    srand(23);
    for (int i = 0; i < n; ++i) {
        r = rand();
        if (max == 0 && min == 0) {
            max = r;
            min = r;
        }
        if (max < r){
            max = r;
        }else if(min > r){
            min = r;
        }

        if (r%2 == 0){
            sum = sum + r;
        }
    }
    int p, k;
    int res0, res1, res2;
    vector<int> koefs;
    cout << "poly power ";
    cin >> p;
    for (int i = 0; i < p+1; ++i) {
        cout << "koef " << i+1 << " ";
        cin >> k;
        koefs.push_back(k);
    }

    res0 = koefs[p];
    for (int k:koefs) {
        res1 = res1 + k*1;
        res2 =  res2 + (k * pow(2, p));
        p = p-1;
    }
    cout << res0 << " " << res1 << " " << res2 << " ";

}
